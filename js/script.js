let add = document.querySelectorAll('.some-class');
let cardOne= localStorage.getItem("one");
let cardTwo= localStorage.getItem("two");
let cardThree= localStorage.getItem("three");
let cardFour= localStorage.getItem("four");
let all = document.querySelectorAll(".card-body");
for (let item of all) {
    let testClasses = item.className.split(" ");
    for (let i = 0; i < testClasses.length; i++) {
        switch (testClasses[i]) {
            case "card1":
                if (localStorage.getItem("one") !== null) {
                    item.innerHTML = cardOne
                }
                break;
            case "card2":
                if (localStorage.getItem("one") !== null) {
                    item.innerHTML = cardTwo
                }
                break;
            case "card3":
                if (localStorage.getItem("one") !== null) {
                    item.innerHTML = cardThree
                }
                break;
            case "card4":
                if (localStorage.getItem("one") !== null) {
                    item.innerHTML = cardFour
                }
                break;
            default:
                ;
        }
    }
}
let ID = function () {
     return '_' + Math.random().toString(36).substr(2, 9);
};
let uniqueKey = ID();
function showInput() {
    this.parentElement.style.display = "none";
    this.parentElement.parentElement.children[3].style.display = "block";
    let addTaskBtn = document.querySelectorAll('.add-task');
    let cancelBtn = document.querySelectorAll(".cancel");
    addTaskBtn.forEach(adder=>  adder.addEventListener('click', addList));
    cancelBtn.forEach(cancel=> cancel.addEventListener('click', cancelAdd));
}
function cancelAdd(){
    this.parentElement.style.display = "none";
    this.parentElement.parentElement.children[2].style.display = "block";
}
    function addList(){
        let addText = this.previousElementSibling;
        let cardBody = this.parentElement.parentElement.children[1];
        let addHeader = this.previousElementSibling.previousElementSibling;
        cardBody.innerHTML += "<li class='card-text border-bottom rounded p-2 bg-white' id="+ uniqueKey +" draggable='true' ondragstart='onDragStart(event);'><button class=\"delete-list\">X</button><h5>" + addHeader.value + "</h5><p>" + addText.value + '</p></li>';
        for (let item of all){
            let testClasses = item.className.split(" ");
            for(let i=0; i<testClasses.length; i++) {
                switch(testClasses[i]) {
                    case "card1":
                        localStorage.setItem("one", item.innerHTML);
                        break;
                    case "card2":
                        localStorage.setItem("two", item.innerHTML);
                        break;
                    case "card3":
                        localStorage.setItem("three", item.innerHTML);
                        break;
                    case "card4":
                        localStorage.setItem("four", item.innerHTML);
                        break;
                    default: ;
                }
            }
        }
       addHeader.value = "";
       addText.value = "";
    }
 add.forEach(adder=> adder.addEventListener('click', showInput));


let deleteListBtn = document.querySelectorAll(".delete-list");
for (let btns of deleteListBtn){
    btns.addEventListener('click', function () {
        console.log(this.parentElement.parentElement.removeChild(this.parentElement))

    })
}
function onDragStart(event) {
    event
        .dataTransfer
        .setData('text/plain', event.target.id);
}

function onDragOver(event) {
    event.preventDefault();
}
function onDrop(event) {
    const id = event
        .dataTransfer
        .getData('text');
    const draggableElement = document.getElementById(id);
    const dropzone = event.target;

    dropzone.appendChild(draggableElement);

    event
        .dataTransfer
        .clearData();
}